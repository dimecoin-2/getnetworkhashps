# Dimecoin 2.0.0

There is nothing to get excited about with this repo. All I have done is taken the original abandoned source code from Github and attempted to add "networkhashps" to it and"automatic p2p" in net.cpp.

- https://github.com/dimecoinproject

I thought it would be a simple matter but Dimecoin is a Quarkcoin clone. Unlike the latter which upgraded, dime is still based on Bitcoin 8.3 and most of the guides were upgrades from 8.6 (and not from Quark clones). 

I called it Dimecoin 2.0.0 because we have Satoshi:1.5.0.14; Satoshi:0.8.3.13; Satoshi:0.8.3.14; and Satoshi:0.9.2.1 out there, and only the first two have source codes published! So dispensing with "Satoshi", we now have "dimecoin:2.0.0" with the source code from Satoshi:0.8.3.13.	

All I have done is made a few small changes (after becoming frustrated that the proper way would not work with dime).

- src/bitcoinrpc.cpp
- src/bitcoinrpc.h
- src/clientversion.h
- src/rpcmining.cpp
- src/rpcrawtransactions
- src/net.cpp

- https://bitbucket.org/dimecoin-2/getnetworkhashps/src/e1b5a4f213ab88d301f2d5c6015a8dcce9e5c792/changes.html?at=master&fileviewer=file-view-default

I updated the checkpoints as well (1.4 million behind), put an "obj" folder in with gitignore and removed "build_config.mk" from the original gitignore (so get rid of it if you do not want it there (src/leveldb)). Last, I changed a few icons for the hell of it.

For a daemon:

make -f makefile.unix

for a qt:

qmake -qt=qt4

make

### Daemons / Servers

Trying to run a wallet that is so bloated with blocks is a hassle. I have no intention to release a Windows version. If the code works, you shoud get an accurate picture of the network activity, not just your pool, hashratespec or mining difficulty. 

Although I am not releasing a Windows wallet (it needs upgrading to Bitcoin 9x), as well as this source code there is a Linux 64 bit Ubuntu-16 pre-compiled daemon and 64 bit Ubuntu-16 pre-compiled wallet.

These may or may not work, as recently I have found pre-compiled Linux QTs and daemons to be a bit hit and miss. 

### Memory issue

The biggest obstacle was strain on memory, or RAM. Whether I caused that or not, I am unsure.

### Pre-compiled 64 bit Linux Ubuntu-16 daemon and wallet

- https://mega.nz/#!wtIVwCYJ!uJPGsEzWbFKYtCVMKi2kNXSGOjRjM672UqWn5XMsX5U
- https://mega.nz/#!RtwmQSqB!hA20UZ1RCKJqbLpo4qosrKlnybWzuhGetBoUrQL_mhA

### Dimecoin configuration

user / rpcuser, password / rpcpassword
        
daemon=1

rpcallowip=127.0.0.1

listen=1

rpcport=11930

port=11931

addnodes 104.131.163.132; 109.235.68.17; 45.32.64.70; 91.121.217.70

For a list of current nodes. 

- https://chainz.cryptoid.info/dime/#!network and 
- http://cryptoguru.tk/NetworkInfo/index.php?Currency=DIME